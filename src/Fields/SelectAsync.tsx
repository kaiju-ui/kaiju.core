import React from 'react'
import {Request} from '../Utils';
import {AsyncPaginate} from 'react-select-async-paginate';

interface SelectInterface {
    closeMenuOnSelect?: boolean
    onChange: any
    isMulti?: boolean
    defaultValue?: any
    style?: any
    className?: any
    value?: any
    required?: boolean
    renderExtraFooter?: any
    renderValue?: any
    container?: any
    cleanable?: boolean | undefined
    placeholder?: string | undefined
    optionHandler: string | undefined
    data?: any[]
    params?: any
    searchable?: boolean | undefined
    disabled?: boolean
    Component?: React.FC
}


const useData = (optionHandler: any, params: any, value?: any) => {
    const [currentValues, setCurrentValues] = React.useState<any | undefined>()
    const [loading, setLoading] = React.useState(false);
    const loadDefault = () => {
        const valueIsArray = Array.isArray(value)
        let nCurrentVal: any = valueIsArray ? [] : undefined
        const setCVal = (val: any) => {
            if (val.options) {
                val.options.map((nVal: any) => {
                    setCVal(nVal)
                })
            }
            if (valueIsArray) {
                if (value.includes(val.value)) nCurrentVal.push(val)
            } else {
                if (val.value === value) nCurrentVal = val
            }
        }
        Request(optionHandler, {...params, id: value}).then((resp: any) => {
            resp.result.data.forEach((val: any) => {
                setCVal(val)
            })
            setCurrentValues(nCurrentVal)
        })
    }

    React.useEffect(() => {
        if (value && value.length > 0) {
            loadDefault()
        }
    }, [value])

    const loadOptions = async (search: any, _loadedOptions: any, meta: any) => {
        const response = await Request(optionHandler, {...params, q: search, ...meta,}).then((resp: any) => {
            return resp.result
        })

        return {
            options: response.data,
            hasMore: response.pagination.page < response.pagination.pages,
            additional: {
                page: meta.page + 1,
            },
        };
    }

    return {
        loading,
        currentValues,
        setCurrentValues,
        loadOptions,
        setLoading,
    }
};


const SelectAsync = (props: SelectInterface) => {
    const state = useData(props.optionHandler, props.params, props.value || props.defaultValue)
    return (
        <AsyncPaginate
            closeMenuOnSelect={props.closeMenuOnSelect}
            placeholder={props.placeholder}
            isMulti={props.isMulti}
            isClearable={true}
            isDisabled={props.disabled}
            isSearchable={true}
            required={props.required}
            value={state.currentValues}
            loadOptions={state.loadOptions}
            onChange={(value: any) => {
                state.setCurrentValues(value)
                props.onChange(value)
            }}
            additional={{
                page: 1,
            }}
        />

    );
};

export {SelectAsync}
