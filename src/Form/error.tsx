import React from "react";
import {Alert} from "antd";

const Error: React.FC<{ message: string }> = ({message}) => {
    return (
        <>
            {message &&
                  <Alert style={{marginTop: "10px"}} message={message} type="error" showIcon />
            }
        </>

    )
};

export {Error};
