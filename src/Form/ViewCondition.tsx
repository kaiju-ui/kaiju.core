import React from "react";

const viewCondition = ({values, viewCondition, children}: any) => {
    if (!viewCondition) {
        return <>{children}</>
    } else {
        const eq = Object.keys(viewCondition).some((key: string) => {
            const currentValue = values[key];
            return currentValue === viewCondition[key]
        });
        return <>{eq && children}</>
    }
};

export default viewCondition;