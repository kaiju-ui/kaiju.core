import {Form as BaseForm} from "./Form"
import * as Fields from './FormFields';
import {Error} from './error';
import {Helper} from "./utils";
import ViewCondition from "./ViewCondition";

import FormStore, {useFormStore} from "./store"
import {idMap, kindMap} from './fields'

const Form = Object.assign(BaseForm, {FormStore, useFormStore, Fields, idMap, kindMap, Error, Helper, ViewCondition})
export {Form}