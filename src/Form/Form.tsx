import React from 'react'
import {FormikProvider} from 'formik';
import {Form as RForm, Flex, Spin} from 'antd';
import {Observer} from 'mobx-react'


const Form: React.FC<{ store: any, children?: any, className?: any, style?: any }> = ({
                                                                                          store,
                                                                                          children,
                                                                                          style,
                                                                                          className
                                                                                      }) => {
    return (
        <Observer>{() => (
            <FormikProvider value={store.formik}>
                <RForm className={className} style={style} layout={"vertical"}>
                    {
                        store.groups.length ? <>
                            {
                                store.groups.map((group: any) => {
                                    return group.fields.map((fieldStore: any) => {
                                        return <fieldStore.Component key={`form-field-${fieldStore.id}`}
                                                                     formik={store.formik} store={store}/>
                                    })
                                })
                            }
                            {children}
                        </> : <Flex align="center" gap="middle">
                            <Spin size="large"/>
                        </Flex>
                    }
                </RForm>
            </FormikProvider>
        )}
        </Observer>
    )
};
export {Form};
