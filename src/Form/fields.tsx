import {
    SingleImageStore,
    TagStore,
    TextBlock,
    JsonObjectStore,
    TextStore,
    NestedListStore,
    NestedStore,
    MultiSelectStore,
    DecimalStore,
    RadioStore,
    SelectAsyncStore,
    IntegerStore,
    SelectStore,
    DateStore,
    DatetimeStore,
    PasswordStore,
    StringStore,
    BooleanStore,
    MultiSelectAsyncStore,
    StringListStore
} from './FormFields'

const kindMap: {[key: string]: any} = {
    string: StringStore,
    text: TextStore,
    password: PasswordStore,
    boolean: BooleanStore,
    integer: IntegerStore,
    select: SelectStore,
    date: DateStore,
    datetime: DatetimeStore,
    select_async: SelectAsyncStore,
    multiselect_async: MultiSelectAsyncStore,
    decimal: DecimalStore,
    multiselect: MultiSelectStore,
    multi_select: MultiSelectStore,
    nested: NestedStore,
    nested_list: NestedListStore,
    json: JsonObjectStore,
    json_object: JsonObjectStore,
    text_block: TextBlock,
    single_image: SingleImageStore,
    string_list: StringListStore,
    tag: TagStore,
    radio: RadioStore,
};

const idMap: {[key: string]: any} = {};

export {idMap, kindMap};
