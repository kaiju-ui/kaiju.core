import React from "react";
import * as Yup from 'yup';
import {getIn} from "formik"
import {Form, Switch} from 'antd';
import {CheckOutlined, CloseOutlined} from '@ant-design/icons';

import FieldStore from './base';
import {Helper} from "../utils";
import ViewCondition from '../ViewCondition'

class BooleanStore extends FieldStore {
    validator: any;

    constructor(props: any) {
        super(props);
        this.value = typeof props.value === 'boolean' ? props.value : this.defaultValue;
        this.validator = Yup.boolean().nullable();
        this.makeValidator(props);
    }

    Component: React.FC = (props: any) => {
        const values = props.values || props.formik.values;
        const _id = this._id(props)
        return (
            <ViewCondition values={values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} tooltip={Helper({comp: this, className: "fs-16"})}>
                    <Switch
                        disabled={this.disabled}
                        checkedChildren={<CheckOutlined/>}
                        unCheckedChildren={<CloseOutlined/>}
                        onChange={(checked: boolean) => {
                            props.formik.setFieldValue(_id, checked)
                        }}
                        checked={getIn(props.formik.values, _id)}
                    />
                </Form.Item>
            </ViewCondition>
        )
    }
}

export default BooleanStore;
