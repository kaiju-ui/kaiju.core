import {Form} from "antd";
import React from "react";
import FieldStore from "./base";
import * as Yup from "yup";
import ViewCondition from "../ViewCondition";
import {Helper} from "../utils";


class UrlStore extends FieldStore {
    validator: any;

    constructor(props: any) {
        super(props);
        this.value = props.value || this.defaultValue;
        this.validator = Yup.object().nullable();
        this.makeValidator(props);
    }

    Component: React.FC = (props: any) => {
        return (
            <ViewCondition values={props.formik.values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                </Form.Item>
            </ViewCondition>
        )

    }
}

export default UrlStore;
