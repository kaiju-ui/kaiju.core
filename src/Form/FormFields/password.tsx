import {Input} from "antd";
import * as Yup from 'yup';
import StringStore from "./string";


class PasswordStore extends StringStore {
    validator: any;
    Comp: any = Input.Password
    constructor(props: any) {
        super(props);
        this.validator = Yup.string();
        this.makeValidator(props)
    }

}

export default PasswordStore;
