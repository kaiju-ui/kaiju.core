import {Form, Alert} from "antd";
import React from "react";
import FieldStore from './base';
import i18n from "i18next";

class TextBlockStore extends FieldStore {
    validator: any;
    text: string | undefined | null;
    textKey: string | undefined | null;
    descriptionKey: string | undefined | null;
    type: "info" | "warning";

    constructor(props: any) {
        super(props);
        this.text = props.text;
        this.textKey = props.text_key;
        this.descriptionKey = props.description_key;
        this.validator = undefined;
        this.type = props.type || "info";
    }

    Component: React.FC = () => {
        return (
            <>
                <Form.Item style={{marginTop: "-20px"}}>
                    <div style={{display: "flex"}}>
                        {(!!this.textKey || !!this.descriptionKey) &&
                            <Alert
                                style={{width: "100%"}}
                                message={this.textKey? i18n.t(this.textKey) : undefined}
                                description={this.descriptionKey? i18n.t(this.descriptionKey) : undefined}
                                type={this.type}
                                showIcon
                            />
                        }
                    </div>
                </Form.Item>
            </>
        )
    }

}

export default TextBlockStore;
