import React from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import {getIn} from "formik"
import {Form, InputNumber} from "antd";
import {Error} from '../error';
import {Helper} from "../utils";
import ViewCondition from "../ViewCondition";


interface DecimalInterface extends FieldStoreInterface {
    max?: number | undefined
    min?: number | undefined
    step?: number | undefined
}

class DecimalStore extends FieldStore {
    validator: any;
    max: any;
    min: any;
    step: any;

    constructor(props: DecimalInterface) {
        super(props);
        this.validator = Yup.number().nullable();
        this.makeValidator(props);
        this.max = props.max;
        this.min = props.min;
        this.step = props.step
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);

        return (
            <ViewCondition values={values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <InputNumber
                        style={{ width: '100%' }}
                        id={this.id}
                        name={this.id}
                        disabled={this.disabled}
                        placeholder={this.placeholder}
                        onChange={(value: any) => {
                            props.formik.setFieldValue(_id, value)
                        }}
                        step={this.step}
                        value={getIn(props.formik.values, _id)}
                        onBlur={props.formik.handleBlur[this.id]}
                        min={this.min}
                        max={this.max}
                    />
                    <Error message={errors[this.id]}/>
                </Form.Item>
            </ViewCondition>
        )
    }

}

export default DecimalStore;
