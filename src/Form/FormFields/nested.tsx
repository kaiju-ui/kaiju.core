import {Form} from "antd";
import React from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import {Helper} from "../utils";
import ViewCondition from "../ViewCondition";


interface IntegerInterface extends FieldStoreInterface {
    fields: any[]

}

class NestedStore extends FieldStore {
    validator: any;
    fields: any[] = [];
    value: { [key: string]: any };

    constructor(props: IntegerInterface) {
        super(props);
        this.value = typeof props.value === 'object' && !Array.isArray(props.value) ? props.value : {};
        this.validator = Yup.object().nullable();
        this.initNested(props.fields);
        this.makeValidator(props);

    }

    initNested(fields: any[]) {
        const _validators: { [key: string]: any } = {};
        fields.forEach((field: any) => {
            const fStore = this.form.kindMap[field.kind] ? this.form.kindMap[field.kind] : undefined;
            if (fStore) {
                const _store = new fStore({
                    form: this.form,
                    parentId: this.id,
                    ...field,
                    loadExtraParams: this.loadExtraParams
                });
                this.fields.push(_store);
                _validators[field.id] = _store.validator;

                if (!(field.id in this.value)) {
                    this.value[field.id] = _store.value
                }
            }
        });
        this.validator = this.validator.shape(
            _validators
        )
    }

    Component: React.FC = (props: any) => {
        return (
            <ViewCondition values={props.formik.values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    {this.fields.map((fieldStore: any) => {
                        return <fieldStore.Component key={`form-field-nested-${fieldStore._id()}`}
                                                     formik={props.formik}
                                                     values={props.formik.values[this.id]}
                                                     errors={props.formik.errors[this.id]}/>
                    })}
                </Form.Item>
            </ViewCondition>
        )
    }
}

export default NestedStore;
