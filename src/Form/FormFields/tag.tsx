import {Form, Select} from "antd";
import React from "react";
import {getIn} from "formik";
import * as Yup from 'yup';
import FieldStore from './base';
import {Error} from '../error';
import {Helper} from "../utils";

class TagStore extends FieldStore {
    validator: any;

    constructor(props: any) {
        super(props);
        this.defaultValue = props.default || [];
        this.value = props.value || this.defaultValue;
        this.validator = Yup.array().nullable();
        this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        // const values = props.values || props.formik.values;
        const _id = this._id(props);

        return (
            <>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <Select
                        mode="tags"
                        style={{width: '100%'}}
                        value={getIn(props.formik.values, _id)}
                        onChange={(value: any) => {
                            props.formik.setFieldValue(_id, value)
                        }}
                        disabled={this.disabled}
                        id={this.id}
                        // options={getIn(props.formik.values, _id)?.map((value: string) =>({value: value}))}
                        // name={_id}
                        // defaultValue={values[this.id]}
                        onBlur={props.formik.handleBlur}
                    />
                    <Error message={errors[this.id]}/>
                </Form.Item>
            </>
        )
    }

}

export default TagStore;
