import {Form} from "antd";
import React from "react";
import * as Yup from 'yup';
import {getIn} from "formik";
import FieldStore, {FieldStoreInterface} from './base';
import {Error} from '../error';

import {Helper} from "../utils";
import {SelectAsync} from "../../Fields/SelectAsync";

interface Interface extends FieldStoreInterface {
    options_handler: string,
    params: any,
}


class SelectAsyncStore extends FieldStore {
    validator: any;
    optionHandler: string;
    params: any;

    constructor(props: Interface) {
        super(props);
        this.optionHandler = props.options_handler;
        this.params = props.params;
        this.defaultValue = props.default || "";
        this.validator = Yup.string().nullable();
        this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const _id = this._id(props);
        return (
            <>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <SelectAsync
                        onChange={(val: any) =>{
                             props.formik.setFieldValue(_id, val ? val.value : val)
                        }}

                        defaultValue={this.defaultValue}
                        disabled={this.disabled}
                        required={this.props.required}
                        value={getIn(props.formik.values, _id)}
                        optionHandler={this.optionHandler}
                        params={{...this.params, ...this.loadExtraParams}}
                    />
                    <Error message={errors[this.id]}/>

                </Form.Item>
            </>
        )
    }

}

export default SelectAsyncStore;
