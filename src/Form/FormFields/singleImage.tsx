import {Form, Upload} from "antd";
import React from "react";
import FieldStore from "./base";
import * as Yup from "yup";
import ViewCondition from "../ViewCondition";
import {Helper} from "../utils";
import type {UploadFile, UploadProps} from 'antd';
import ImgCrop from 'antd-img-crop';
import {Error} from "../error";
import i18next from "i18next";

const toBase64 = (file: any) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
});


class SingleImageStore extends FieldStore {
    validator: any;
    maxSize: number | undefined | null;

    constructor(props: any) {
        super(props);
        this.value = props.value || this.defaultValue;
        this.maxSize = props.maxSize;
        this.validator = Yup.mixed().nullable()
        this.makeValidator(props);

    }

    makeValidator = (props: any) => {
        if (!this.validator) return;

        if (props.required) {
            this.validator = this.validator.required(i18next.t('field.error.required') || 'Required')
        }

        // if (props.maxSize) {
        //     let maxFile = i18next.t('field.error.max-file-size') || 'Max file size {max} MB';
        //     maxFile = maxFile.replace("{max}", props.maxSize);
        //
        //     this.validator = this.validator.test('file-size',
        //         maxFile, (value: any, context: any) => {
        //             const {path, createError} = context;
        //             return (!value || (value && value.size <= 1024 * 1024 * props.maxSize)) || createError({
        //                 path,
        //                 message: maxFile
        //             })
        //         });
        // }

    }

    beforeUpload = (file: any) => {
        const _id = this._id(this.props);
        let isLg = false
        if (this.maxSize) {
            isLg = file.size / 1024 / 1024 < this.maxSize;
            if (!isLg) {
                let maxFile = i18next.t('field.error.max-file-size') || 'Max file size {max} MB';
                maxFile = maxFile.replace("{max}", `${this.maxSize}`);
                this.props.form.formik.setFieldError(_id, maxFile)

            }
        }

        return isLg;
    };

    Component: React.FC = (props: any) => {
        const [fileList, setFileList] = React.useState<UploadFile[]>([])
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;

        const _id = this._id(props);
        const onChange: UploadProps['onChange'] = ({fileList}: any) => {
            setFileList(fileList);
        };


        return (
            <ViewCondition values={values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <ImgCrop rotationSlider>
                        <Upload
                            listType="picture-card"
                            fileList={fileList}
                            maxCount={1}
                            multiple={false}
                            beforeUpload={this.beforeUpload}
                            customRequest={(info: any) => {
                                toBase64(info.file).then((data: any) => {
                                    props.formik.setFieldValue(_id, data)
                                    info.onSuccess()
                                })
                                return true
                            }}
                            onRemove={() => {
                                props.formik.setFieldValue(_id, null)
                            }}
                            onChange={onChange}
                        >
                            {fileList.length === 0 && '+ Upload'}
                        </Upload>
                    </ImgCrop>
                    <Error message={errors[this.id]}/>

                </Form.Item>
            </ViewCondition>
        )
    }
}

export default SingleImageStore;
