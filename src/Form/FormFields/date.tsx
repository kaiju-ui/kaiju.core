import React from "react";
import * as Yup from 'yup';
import dayjs from 'dayjs';
import FieldStore, {FieldStoreInterface} from './base';
import {getIn} from "formik";
import {Error} from '../error';
import {Helper} from "../utils";
import ViewCondition from "../ViewCondition";
import {Form, DatePicker} from "antd";


class DateStore extends FieldStore {
    validator: any;
    format_: string;

    constructor(props: FieldStoreInterface) {
        super(props);
        this.validator = Yup.date().nullable();
        this.makeValidator(props);
        this.format_ = this.props.format_ || 'YYYY-MM-DD'
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const _id = this._id(props);
        return (
            <ViewCondition values={getIn(props.formik.values, _id)} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <DatePicker
                        style={{width: '100%'}}
                        id={_id}
                        name={_id}
                        format={this.format_}
                        disabled={this.disabled}
                        placeholder={this.placeholder}
                        onChange={(_: any, dateString: any) => {
                            props.formik.setFieldValue(_id, dateString? dayjs(dateString, this.format_).format('YYYY-MM-DD') : null)
                        }}
                        value={getIn(props.formik.values, _id)? dayjs(getIn(props.formik.values, _id)): undefined }
                        onBlur={props.formik.handleBlur[this.id]}
                    />
                    <Error message={errors[this.id]}/>
                </Form.Item>
            </ViewCondition>
        )
    }
}

export default DateStore;
