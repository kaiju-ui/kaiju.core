import i18n from "i18next";

export interface FieldStoreInterface {
    id: string
    parentId?: string
    form: any
    label?: any
    disabled?: boolean
    value?: any
    placeholder: string | undefined
    default: string | undefined
    helper_key: string | undefined
    helper_text: string | undefined
    label_key?: string | undefined
    loadExtraParams?: any
}


class FieldStore {
    id: string;
    label: string;
    parentId?: string;
    form: any;
    defaultValue: any;
    helperKey: any;
    helperText: any;
    value: any;
    disabled?: boolean;
    placeholder: any;
    props: any;
    validator: any;
    loadExtraParams?: any;

    constructor(props: FieldStoreInterface) {
        this.props = props;
        this.id = props.id;
        this.parentId = props.parentId;
        this.form = props.form;
        this.loadExtraParams = props.loadExtraParams;
        this.defaultValue = props.default;
        this.value = props.value || this.defaultValue;
        this.placeholder = props.placeholder;
        this.disabled = props.disabled;
        this.helperKey = props.helper_key;
        this.helperText = props.helper_text;
        // this.validator = Yup.string();
        // this.makeValidator(props);
        this.label = props.label ? props.label : props.label_key ? i18n.t(props.label_key) : this.id;
    }

    _id = (props: any) => {
        const index = props?.index;
        let resp_id: any = this.parentId ? this.parentId : undefined
        if (resp_id && index !== undefined){
            resp_id = resp_id + "." + index
        }
        if (!resp_id){
            resp_id = props?.parentId
        }
        resp_id = resp_id? resp_id + "." + this.id : this.id;
        return resp_id
    };

    makeValidator = (props: any) => {
        if (!this.validator) return;

        if (props.required) {
            this.validator = this.validator.required(i18n.t('field.error.required') || 'Required')
        }
        if (props.min) {
            let minText = i18n.t('field.error.short') || 'Minimal length is {min}';
            minText = minText.replace("{min}", props.min);
            this.validator = this.validator.min(props.min, minText)
        }
        if (props.max) {
            let minText = i18n.t('field.error.long') || 'Max length is {min}';
            minText = minText.replace("{max}", props.max);
            this.validator = this.validator.max(props.max, minText)
        }
        if (props.reg_exp) {
            this.validator = this.validator.matches(props.reg_exp, props.reg_exp_text)
        }
        if (props.regex) {
            this.validator = this.validator.matches(props.regex.pattern, i18n.t(props.regex.msg))
        }

    }
}

export default FieldStore;
