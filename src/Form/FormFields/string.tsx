import {Form, Input} from "antd";
import React from "react";
import * as Yup from 'yup';
import FieldStore from './base';
import {Error} from '../error';
import {Helper} from "../utils";

class StringStore extends FieldStore {
    validator: any;
    mask?: any[] | undefined;
    Comp: any = Input;

    constructor(props: any) {
        super(props);
        this.defaultValue = props.default || "";
        this.validator = Yup.string().nullable();
        this.makeValidator(props);
        this.mask = props.mask
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);

        const extraParams: { [key: string]: any } = {};
        if (this.mask) {
            extraParams["placeholderChar"] = " "
        }

        return (
            <>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <this.Comp
                        {...extraParams}
                        count={{
                            show: typeof this.props.max === 'number',
                            max: this.props.max,
                        }}
                        // mask={this.mask}
                        id={this.id}
                        name={this.id}
                        disabled={this.disabled}
                        placeholder={this.placeholder}
                        onChange={(val: any) => {
                            props.formik.setFieldValue(_id, val ? val.target.value : val)
                        }}
                        value={values[this.id]}
                        onBlur={props.formik.handleBlur}
                    />
                    <Error message={errors[this.id]}/>
                </Form.Item>
            </>
        )
    }

}

export default StringStore;
