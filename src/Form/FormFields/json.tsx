import {Form as BaseForm} from "antd";
import React from "react";
import ReactJson from '@microlink/react-json-view'
import FieldStore from './base';
import {Helper} from "../utils";
import {Error} from "../error";

class JsonObjectStore extends FieldStore {
    validator: any;
    readOnly: boolean = false;

    constructor(props: any) {
        super(props);
        this.validator = undefined
        this.defaultValue = props.field_type === 'list' ? [] : {}
        this.readOnly = props.read_only || false
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);
        const _value = values[this.id] || this.defaultValue;
        const jsonProps: { [key: string]: any } = {}

        if (!this.readOnly) {
            jsonProps["onAdd"] = (e: any) => {
                props.formik.setFieldValue(_id, e.updated_src)
            };

            jsonProps["onEdit"] = (e: any) => {
                props.formik.setFieldValue(_id, e.updated_src)
            };
            jsonProps["onDelete"] = (e: any) => {
                props.formik.setFieldValue(_id, e.updated_src)
            };
        }

        return (
            <>
                <BaseForm.Item label={this.label} required={this.props.required}
                               tooltip={Helper({comp: this, className: "fs-16"})}>
                    <ReactJson src={_value} displayDataTypes={false} displayObjectSize={false} name={null}
                               {...jsonProps}
                    />
                </BaseForm.Item>
                <Error message={errors[this.id]}/>
            </>
        )
    }

}

export default JsonObjectStore;
