import * as Yup from 'yup';
import SelectStore from "./select"
import {SelectProps} from "./select";

class MultiSelectStore extends SelectStore {
    constructor(props: SelectProps) {
        super(props);
        this.options = props.options;
        this.defaultValue = props.default || [];
        this.validator = Yup.array().of(Yup.string()).nullable();
        this.makeValidator(props)
        this.mode = "multiple"
    }

}

export default MultiSelectStore;
