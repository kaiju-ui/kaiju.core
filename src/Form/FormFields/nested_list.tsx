import {Form} from "antd";
import React from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import ViewCondition from "../ViewCondition";
import {Helper} from "../utils";

interface Interface extends FieldStoreInterface {
    fields: any[]

}

class NestedListStore extends FieldStore {
    validator: any;
    fields: any[] = [];
    value: any[];

    constructor(props: Interface) {
        super(props);
        this.value = Array.isArray(props.value) ? props.value : [];
        this.validator = Yup.array().nullable();
        this.initNested(props.fields);
        this.makeValidator(props);

    }

    initNested(fields: any[]) {
        const _validators: { [key: string]: any } = {};

        fields.forEach((field: any) => {
            const fStore = this.form.kindMap[field.kind] ? this.form.kindMap[field.kind] : undefined;
            if (fStore) {
                const _store = new fStore({
                    form: this.form,
                    parentId: this.id,
                    ...field,
                    loadExtraParams: this.loadExtraParams
                });
                this.fields.push(_store);
                _validators[field.id] = _store.validator;
                this.value.forEach(val => {
                    if (!(field.id in val)) {
                        val[field.id] = _store.value
                    }
                })
            }
        });
        this.validator = this.validator.of(
            Yup.object().shape(
                _validators
            )
        )
    }

    emptyValue = () => {
        const resp: { [key: string]: any } = {};
        this.fields.forEach((val => {
            resp[val.id] = val.value;
        }));
        return resp
    };

    Component: React.FC = (props: any) => {
        // const [state, setState] = React.useState((new Date()).toISOString());
        // const moveCard = useCallback((move: any, dragIndex: number, hoverIndex: number) => {
        //     console.log("\n>>", dragIndex, hoverIndex)
        //     console.log("\n")
        //     move(dragIndex, hoverIndex)
        //
        // }, [])

        return (
            <ViewCondition values={props.formik.values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    THIS IS NESTED LIST
                </Form.Item>
            </ViewCondition>
        )
    }

}

export default NestedListStore;
