import {Form, Radio, Space} from "antd";
import React from "react";
import {getIn} from "formik";
import SelectStore from "./select";
import ViewCondition from "../ViewCondition";
import i18n from "i18next";
import {Helper} from "../utils";

interface Option {
    value: string
    label: string
    disabled?: boolean
}

class RadioStore extends SelectStore {
    Component: React.FC = (props: any) => {
        const values = props.values || props.formik.values;
        const _id = this._id(props);

        return (
            <ViewCondition values={values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>

                    <Radio.Group disabled={this.disabled} value={getIn(this.form.formik.values, _id)}
                                 onChange={(value: any) => {
                                     props.formik.setFieldValue(_id, value.target.value)
                                 }}>
                        <Space direction="vertical">

                            {this.options.map((option: Option) => (
                                <Radio style={{color: "rgba(0, 0, 0, 0.88)"}} disabled={option.disabled}
                                       value={option.value}>{i18n.t(option.label)}</Radio>
                            ))}</Space>
                    </Radio.Group>

                </Form.Item>
            </ViewCondition>
        )
    }
}

export default RadioStore;
export type {Option}
