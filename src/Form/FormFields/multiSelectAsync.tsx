import {Form} from "antd";
import React from "react";
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import {Error} from '../error';

import {Helper} from "../utils";
import {SelectAsync} from "../../Fields/SelectAsync";
import {getIn} from "formik";

interface Interface extends FieldStoreInterface {
    options_handler: string,
    params: any,
}


class MultiSelectAsyncStore extends FieldStore {
    validator: any;
    optionHandler: string;
    params: any;

    constructor(props: Interface) {
        super(props);
        this.optionHandler = props.options_handler;
        this.params = props.params;
        this.defaultValue = props.default || [];
        this.validator = Yup.array().of(Yup.string()).nullable();
        this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const _id = this._id(props);
        return (
            <>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <SelectAsync
                        closeMenuOnSelect={false}
                        onChange={(val: any[]) => {
                            const newVal = val? val.map((el: any)=> el.value) : []
                            props.formik.setFieldValue(_id, newVal)
                        }}
                        isMulti={true}
                        defaultValue={this.defaultValue}
                        disabled={this.disabled}
                        required={this.props.required}
                        value={getIn(props.formik.values, _id)}
                        optionHandler={this.optionHandler}
                        params={{...this.params, ...this.loadExtraParams}}
                    />
                    <Error message={errors[this.id]}/>

                </Form.Item>
            </>
        )
    }

}

export default MultiSelectAsyncStore;
