import {Form} from "antd";
import React from "react";
import FieldStore from "./base";
import * as Yup from "yup";
import ViewCondition from "../ViewCondition";
import {Helper} from "../utils";

import {Error} from "../error";
import i18next from "i18next";


class ImageStore extends FieldStore {
    validator: any;

    constructor(props: any) {
        super(props);
        this.value = props.value || this.defaultValue;
        this.validator = Yup.mixed().nullable()
        this.makeValidator(props);

    }

    makeValidator = (props: any) => {
        if (!this.validator) return;

        if (props.required) {
            this.validator = this.validator.required(i18next.t('field.error.required') || 'Required')
        }

        if (props.max) {
            let maxFile = i18next.t('field.error.max-file-size') || 'Max file size {max} MB';
            maxFile = maxFile.replace("{max}", props.max);

            this.validator = this.validator.test('',
                maxFile, (value: any) => {
                    return !value || (value && value.size <= 1024 * 1024 * props.max)
                });
        }

    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;

        return (
            <ViewCondition values={props.formik.values} viewCondition={this.props.view_condition}>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    IMAGE
                    <Error message={errors[this.id]}/>

                </Form.Item>
            </ViewCondition>
        )
    }
}

export default ImageStore;
