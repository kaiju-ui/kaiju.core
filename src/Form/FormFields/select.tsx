import {Form, Select} from "antd";
import React from "react";
import {getIn} from "formik"
import * as Yup from 'yup';
import FieldStore, {FieldStoreInterface} from './base';
import {Error} from '../error';
import {Helper} from "../utils";

interface OptionProps {
    value: string
    label: string
}

interface SelectProps extends FieldStoreInterface {
    options: OptionProps[],
}


class SelectStore extends FieldStore {
    validator: any;
    options: OptionProps[];
    mode?: 'multiple' | 'tags'

    constructor(props: SelectProps) {
        super(props);
        this.options = props.options;
        this.defaultValue = props.default || "";
        this.validator = Yup.string().nullable();
        this.makeValidator(props)
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        // const values = props.values || props.formik.values;
        const _id = this._id(props);
        return (
            <>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <Select
                        mode={this.mode}
                        value={getIn(props.formik.values, _id)}

                        id={this.id}
                        allowClear
                        disabled={this.disabled}
                        placeholder={this.placeholder}
                        onChange={(val: any) => {
                            props.formik.setFieldValue(_id, val)
                        }}
                        onBlur={props.formik.handleBlur[this.id]}
                        options={this.options}
                    />
                    <Error message={errors[this.id]}/>
                </Form.Item>
            </>
        )
    }

}

export default SelectStore;
export type {SelectProps}
