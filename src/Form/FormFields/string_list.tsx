import React from "react";
import {Form, Input, Divider} from "antd";
import {CloseOutlined, DragOutlined, PlusOutlined} from "@ant-design/icons"
import {ReactSortable} from "react-sortablejs";
import {getIn} from "formik";
import * as Yup from 'yup';
import i18n from "i18next";
import FieldStore from './base';
import {Error} from '../error';
import {Helper} from "../utils";


class StringListStore extends FieldStore {
    validator: any;
    mask?: any[] | undefined;

    constructor(props: any) {
        super(props);
        this.defaultValue = Array.isArray(props.default) ? props.default : [];
        this.validator = Yup.array().of(Yup.string()).nullable();
        this.makeValidator(props);
        this.mask = props.mask
    }

    Component: React.FC = (props: any) => {
        const errors = props.errors || props.formik.errors;
        const values = props.values || props.formik.values;
        const _id = this._id(props);

        const extraParams: { [key: string]: any } = {};
        if (this.mask) {
            extraParams["placeholderChar"] = " "
        }
        const [newValue, setNewValue] = React.useState<string>()

        return (
            <>
                <Form.Item label={this.label} required={this.props.required}
                           tooltip={Helper({comp: this, className: "fs-16"})}>
                    <Divider style={{marginTop: 5}}/>

                    <>
                        <ReactSortable
                            animation={200}
                            list={getIn(props.formik.values, _id) || []}
                            setList={(newState) => {
                                props.formik.setFieldValue(_id, [...newState])
                            }}
                        >

                            {(getIn(props.formik.values, _id) && getIn(props.formik.values, _id).length > 0) && getIn(props.formik.values, _id).map((value: string, index: number) => {
                                const name = _id + "." + index
                                return (
                                    <Input
                                        name={name}
                                        style={{marginBottom: 10}}
                                        placeholder={"fields.add"}
                                        value={value}
                                        key={"tag-" + name}
                                        prefix={
                                            <DragOutlined/>
                                        }
                                        onBlur={props.formik.handleBlur}
                                        onChange={(event: any) => {
                                            props.formik.setFieldValue(name, event.target.value);
                                        }}
                                        suffix={
                                            <CloseOutlined style={{cursor: "pointer"}}
                                                           onClick={() => {
                                                               const currentVal = getIn(props.formik.values, _id)
                                                               currentVal.splice(index, 1)
                                                               props.formik.setFieldValue(_id, [...currentVal])
                                                           }}
                                            />
                                        }
                                    />
                                )
                            })}
                        </ReactSortable>

                        <Input
                            {...extraParams}
                            width={100}
                            count={{
                                show: typeof this.props.max === 'number',
                                max: this.props.max,
                            }}
                            id={this.id}
                            name={this.id}
                            disabled={this.disabled}
                            placeholder={i18n.t("fields.add") || ""}
                            value={newValue}
                            prefix={<PlusOutlined/>}
                            addonAfter={
                                <div
                                    style={{cursor: "pointer"}}
                                    onClick={() => {
                                        const currentVal = getIn(values, _id) || []
                                        currentVal.push(newValue)
                                        props.formik.setFieldValue(_id, currentVal)
                                        setNewValue(undefined)
                                    }}
                                >{i18n.t("common.add")}</div>
                            }
                            onPressEnter={() => {
                                const currentVal = getIn(values, _id) || []
                                currentVal.push(newValue)
                                props.formik.setFieldValue(_id, currentVal)
                                setNewValue(undefined)
                            }}
                            onChange={(val: any) => {
                                setNewValue(val.target.value)
                            }}
                        />
                    </>

                    <Error message={errors[this.id]}/>
                </Form.Item>
            </>
        )
    }

}

export default StringListStore;
