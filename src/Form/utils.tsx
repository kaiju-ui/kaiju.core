import i18n from "i18next";
import React from "react";

const Helper = (props: any) => {
    const {comp, className} = props;
    if (comp.helperKey) {
        return <div className={className}>{i18n.t(comp.helperKey)}</div>
    }
    if (comp.helperText && !comp.helperKey) {
        return <div className={className}>{comp.helperText}</div>
    }
};

export {Helper}
