import * as React from "react";

interface Interface {
    userPermission: string[];
    children: any;
    permission: string[];
    redirectTo?: string;
}

const Permission = (props: Interface) => {
    return (
        <>
            {props.permission.some(v => props.userPermission.includes(v)) && props.children}
        </>
    )
};

export {Permission}