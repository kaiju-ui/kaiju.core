import React from "react";
import i18n from "i18next";
import {App} from 'antd';
import {useFormik} from "formik";

import {Form} from "../Form";
import {Request} from "../Utils"


interface RequestInterface {
    method: string,
    params: { [key: string]: any }
}

interface DFInterface {
    disableGroup?: boolean;
    loadRequest: RequestInterface;
    loadExtraParams?: any;
    saveRequest: RequestInterface;
    successCallback: (props: any) => void;
    errorCallback?: (props: any) => void;
    onLoad?: (props: any) => void;
    autoLoad?: boolean;
    notification?: any;
    saveDiff?: boolean;
    isNested?: boolean;

}

class DynamicFormStore {
    disableGroup?: boolean;
    loadRequest: RequestInterface;
    loadExtraParams?: any;
    saveRequest: RequestInterface;
    successCallback: any;
    errorCallback?: any;
    formStore: any;
    onLoad?: (props: any) => void;
    notification?: any;
    autoLoad: boolean = false;
    isNested: boolean = false;
    saveDiff?: boolean = false;

    constructor({saveDiff = true, ...props}: DFInterface) {
        /* {
            disableGroup: true,
            loadRequest: {
                method: 'Dome method',
                params: {
                    param1: "PARAM 1"
                }
            },
            loadExtraParams: {},
            saveRequest: {
                method: 'Dome method',
                params: {
                    formParam: "PARAM 1"
                }
            },
            successCallback: (response) => {
                console.log(response)
            }
         }
        */

        this.disableGroup = props.disableGroup || false;
        this.notification = props.notification;
        this.isNested = props.isNested || this.isNested;
        this.saveDiff = saveDiff;
        this.loadRequest = props.loadRequest;
        this.loadExtraParams = props.loadExtraParams;
        this.saveRequest = props.saveRequest;
        this.successCallback = props.successCallback;
        this.errorCallback = props.errorCallback;
        this.onLoad = props.onLoad;
        this.autoLoad = props.autoLoad || this.autoLoad;
        this.formStore = new Form.FormStore({
            fields: [],
            loadExtraParams: this.loadExtraParams,
            disableGroups: true,
            onSubmit: (values: any, formikState: any, changedValues: any) => {
                this.save(values, formikState, changedValues)
            }
        });
        if (this.autoLoad) {
            this.load()
        }
    }

    load() {
        Request(this.loadRequest.method, this.loadRequest.params).then((response: any) => {
            if (response.result) {
                const fields = response.result.map((group: any) => {
                    return {
                        id: group.id || group.key,
                        ...group
                    }
                });
                const valStructure = this.formStore.initFields(fields);
                if (this.onLoad) {
                    this.onLoad(valStructure)
                }
            }
        })
    }

    save(values: any, formikState: any, changedValues: any) {
        let params = {}
        if (this.saveDiff) {
            params = {
                ...this.saveRequest.params,
                ...changedValues
            };
        } else {
            params = {
                ...this.saveRequest.params,
                ...values
            };
        }
        if (this.isNested) {
            params = {data: params}
        }

        Request(this.saveRequest.method, params).then((response: any) => {
            if (response.error?.code === 422 || response.error?.code === -32602) {
                const errors: { [key: string]: string[] } = {};

                Object.keys(response.error?.data?.fields || {}).forEach((key: any) => {
                    const fieldsErrors: any[] = response.error?.data?.fields[key];
                    const fullErrors: string[] = [];
                    fieldsErrors.forEach((error => {
                        fullErrors.push(i18n.t(error.code).toString())
                    }));
                    errors[key] = fullErrors
                });
                formikState.setErrors(errors);

                if (this.notification) {
                    this.notification.error({
                        message: i18n.t('error.validation-error-title'),
                        placement: 'topRight',
                    });
                }
            } else if (response.error) {
                if (this.notification) {
                    this.notification.error({
                        message: i18n.t('error.internal-error'),
                        placement: 'topRight',
                    });
                }
                if (this.errorCallback) {
                    this.errorCallback(response, this)
                }

            } else if (response.result) {
                this.successCallback(response, this)
            }
        })
    }

    clear() {
        this.formStore.clear()
    }

    refresh() {
        this.formStore.formik.setValues(this.formStore.formik.initialValues)
    }

    submit() {
        this.formStore.submit()
    }

}

const useDynamicForm = (props: DFInterface) => {
    // Toaster for messages
    // const toaster = useToaster();
    const {notification} = App.useApp();
    const [isLoad, setLoad] = React.useState<boolean>(false)
    const onLoad = (vals: any) => {
        setLoad(true)
        if (props.onLoad) props.onLoad(vals)
    }
    const [dfStore] = React.useState(() => {
        return new DynamicFormStore({...props, onLoad, notification})
    });
    const init: any = {
        ...dfStore.formStore.init(),
        enableReinitialize: true,
        initialValues: isLoad ? dfStore.formStore.init().initialValues : {}
    }
    let formik = useFormik(init);
    dfStore.formStore.setFormik(formik);
    return [dfStore]
};

export {DynamicFormStore, useDynamicForm}
export type {RequestInterface, DFInterface}